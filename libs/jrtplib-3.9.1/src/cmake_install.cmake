# Install script for directory: /home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/jrtplib3" TYPE FILE FILES
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcpapppacket.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcpbyepacket.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcpcompoundpacket.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcpcompoundpacketbuilder.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcppacket.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcppacketbuilder.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcprrpacket.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcpscheduler.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcpsdesinfo.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcpsdespacket.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcpsrpacket.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtcpunknownpacket.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpaddress.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpcollisionlist.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpconfig.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpdebug.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpdefines.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtperrors.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtphashtable.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpinternalsourcedata.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpipv4address.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpipv4destination.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpipv6address.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpipv6destination.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpkeyhashtable.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtplibraryversion.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpmemorymanager.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpmemoryobject.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtppacket.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtppacketbuilder.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtppollthread.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtprandom.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtprandomrand48.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtprandomrands.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtprandomurandom.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtprawpacket.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpsession.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpsessionparams.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpsessionsources.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpsourcedata.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpsources.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpstructs.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtptimeutilities.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtptransmitter.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtptypes_win.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtptypes.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpudpv4transmitter.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpudpv6transmitter.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpbyteaddress.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/rtpexternaltransmitter.h"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/extratransmitters/rtpfaketransmitter.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/lib/libjrtp.a")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/lib" TYPE STATIC_LIBRARY FILES "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/libjrtp.a")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FOREACH(file
      "$ENV{DESTDIR}/usr/local/lib/libjrtp.so.3.9.1"
      "$ENV{DESTDIR}/usr/local/lib/libjrtp.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      FILE(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    ENDIF()
  ENDFOREACH()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/lib/libjrtp.so.3.9.1;/usr/local/lib/libjrtp.so")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/local/lib" TYPE SHARED_LIBRARY FILES
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/libjrtp.so.3.9.1"
    "/home/vagrant/sirannon-1.0.0/libs/jrtplib-3.9.1/src/libjrtp.so"
    )
  FOREACH(file
      "$ENV{DESTDIR}/usr/local/lib/libjrtp.so.3.9.1"
      "$ENV{DESTDIR}/usr/local/lib/libjrtp.so"
      )
    IF(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      IF(CMAKE_INSTALL_DO_STRIP)
        EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "${file}")
      ENDIF(CMAKE_INSTALL_DO_STRIP)
    ENDIF()
  ENDFOREACH()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

